
# Import the library
import grew

# Run the grew tool in background
grew.init(True) # local usage --> run grewpy_dev

# Load the corpus file (of course, you will have to update the filename to your system)
c = grew.corpus ("/users/guillaum/gitlab/deep-sequoia/trunk/sequoia.deep_and_surf.conll")

# Search for an empty pattern and print the number of occurences
r = grew.corpus_search ("pattern {}", c)
print (len (r))