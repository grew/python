import sys
import grew

grew.init(True) # local usage --> run grewpy_dev

gsd = grew.corpus (["UD_French-GSD/fr_gsd-ud-dev.conllu", "UD_French-GSD/fr_gsd-ud-test.conllu", ])

# graph = grew.corpus_get (gsd, "fr-ud-dev_00002")
# print(graph)

print (grew.corpus_size (gsd))

ids = grew.corpus_sent_ids(gsd)
print (ids[10:20])

try:
    graph = grew.corpus_get ("fr-ud-dev_00002", gsd)
    svg = grew.graph_svg(graph)
    print(svg)
except grew.utils.GrewError as err:
    print ("ERROR: %s" % err)


