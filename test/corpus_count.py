import grew
import sys
import json

grew.init(True) # local usage --> run grewpy_dev

seq = grew.corpus(["UD_French-GSD/fr_gsd-ud-test.conllu", "UD_French-GSD/fr_gsd-ud-dev.conllu", "UD_French-GSD/fr_gsd-ud-train.conllu"])


print ("|NOUN| = " + json.dumps (grew.corpus_count("pattern { N[upos=NOUN] }", seq)))
print ("|NOUN| -[cop]-> = " + json.dumps (grew.corpus_count("pattern { N[upos=NOUN]; N -[cop]-> * }", seq)))
print (" -[nsubj]-> |NOUN| = " + json.dumps (grew.corpus_count("pattern { * -[nsubj]-> N;  N[upos=NOUN]; }", seq)))

