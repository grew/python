import grew
import granalysis
import sys

grew.init()

print("============ Build a graph =================")
jd = grew.graph(['Noé', 'dort'])
print(jd)
print("============ Elle_pense_venir.gr =================")
Elle_pense_venir = grew.graph('data/ipv.gr')
print(Elle_pense_venir)


print("============ Run grew ======================")
grs_id = grew.grs("data/test.grs")
print("======> id=%s" % grs_id)
rewritten_gr = grew.run(grs_id,Elle_pense_venir)

print("============ Graph rewritten_gr ============")
print(rewritten_gr)

print("============ Draw a graph ==================")
#grew.draw(jd)


print("============ Add edges to a graph ==================")

grew.add_edge(jd, 'W0', 'SUJ', 'W1')
grew.add_edge(jd, 'W0', 'SUJ', 'W1')  # not useful, but no harms
#grew.add_edge(jd, 'W0', 'OBJ', 'W0') # loop are not handled by dep2pict
grew.add_edge(jd, 'W0', 'OBJ', 'W1')
print(jd)

print("============ Add nodes to a graph ==================")
jndp = grew.graph(jd)
neid = grew.insert_before(jndp, "ne", 'W1')
pasid = grew.insert_after(jndp, "pas", 'W1')
grew.add_edge(jndp, 'W1', 'ADV', neid)
grew.add_edge(jndp, 'W1', 'ADV', pasid)
#grew.draw(jndp)
print(jndp)
# print(jd)

print("============ Draw a graph ==================")
#grew.draw(jndp,"dep")
# pattern = "match { G->D; }"

# matching_list = grew.search (pattern, jndp)
# print("found %d matchings of the pattern >>>%s<<< :" % (len(matching_list), pattern))
# for d in matching_list:
# print (d)


print("============ Pattern matching ==================")

#latex grewchild
g = grew.graph('''graph{
W1 [phon="the", cat=DET];
W2 [phon="child", cat=N];
W3 [phon="plays", cat=V];
W4 [phon="the", cat=DET];
W5 [phon="fool", cat=N];
W2 -[det]->W1;
W3 -[suj]->W2;
W3 -[obj]->W5;
W5 -[det]->W4;
}''')
#/latex
print(g)

#latex match_node
grew.search ("pattern { X [cat=V] }", g)
#eval latex
#/latex

#latex several_sol
grew.search ("pattern { X [cat=DET] }", g)
#eval latex
grew.search ("pattern { X [cat=ADJ] }", g)
#eval latex
#/latex

#latex match_edge
grew.search ("pattern { X[cat=V]; Y[]; X -[suj]-> Y }", g)
#eval latex
#/latex
#print ("pattern { X[cat=V]; Y[]; X -[suj]-> Y } : %s")

#latex match_edge_bis
grew.search ("pattern { X[cat=V]; X -[suj]-> Y }", g)
#eval latex
#/latex

#latex match_with_edge_name
grew.search ("pattern { X[cat=V]; e:X -[suj]-> Y }", g)
#eval latex
#/latex
#print ("pattern {X[cat=V]; e:X -[suj]-> Y} : %s" % m)

#latex root_in_grew
grew.search("pattern { X[] } without { *->X }", g)
#eval latex
#/latex

print("============ json ==================")
print (grew.json(grs_id, "test.json"))

print("============ Termination ==================")
print("Rewriting is terminating for g: %s\n" % granalysis.is_terminating(grs_id,grew.graph2gr(g)))

print("bye...")
