PREFIX?=/usr/local
BINDIR=$(PREFIX)/bin

OCB_FLAGS = -use-ocamlfind -I src_ocaml
OCB = ocamlbuild $(OCB_FLAGS)

VERSION = `cat VERSION`

default: native

native:
	$(OCB) -tag-line "true:	package(libgrew)" grewpy.native

install: native
	mkdir -p $(BINDIR)
	cp grewpy.native $(BINDIR)/grewpy

uninstall:
	rm -f $(BINDIR)/grewpy

.PHONY:	all clean byte native install uninstall

clean:
	$(OCB) -clean

info:
	@echo "BINDIR = $(BINDIR)"
